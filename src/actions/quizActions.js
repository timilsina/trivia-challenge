export const SET_QUESTIONS = 'SET_QUESTIONS';
export const SET_ANSWER = 'SET_ANSWER';
export const INCREASE_INDEX = 'INCREASE_INDEX';
export const SET_FETCHING = 'SET_FETCHING';

export function setQuestions(payload) {
    return { type: SET_QUESTIONS, payload };
}

export function increaseIndex() {
    return { type: INCREASE_INDEX };
}

export function setAnswer(payload) {
    return { type: SET_ANSWER, payload };
}

export function setFetching() {
    return { type: SET_FETCHING };
}

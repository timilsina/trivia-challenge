import React, { Component } from 'react';
import { Text } from 'react-native';
import { AllHtmlEntities } from 'html-entities';
import PropTypes from 'prop-types';

const entities = new AllHtmlEntities();

class HtmlEntityComponent extends Component {

    render() {
        let { question, style } = this.props;
        return (
            <Text style={style}>{entities.decode(question)}</Text>
        );
    }

    static propTypes = {
        style: PropTypes.number,
        question: PropTypes.string.isRequired
    }
}

export default HtmlEntityComponent;

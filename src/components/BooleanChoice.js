import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import { Button, Text } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    option: {
        marginVertical: 10,
        width: 90,
    },
    trueOption: {
        backgroundColor: '#66ce41'
    },
    falseOption: {
        backgroundColor: '#e92b2b'
    },
    optionText: {
        flex: 1,
        textAlign: 'center',
    }
});
class BooleanChoice extends Component {

    render() {
        return (
            <View style={this.props.style}>
                <Button style={[styles.option, styles.trueOption]} onPress={() => this.props.onChoice('True')} success>
                    <Text style={styles.optionText}>True</Text>
                </Button> 
                <Button style={[styles.option, styles.falseOption]} onPress={() => this.props.onChoice('False')} danger>
                    <Text style={styles.optionText}>False</Text>
                </Button> 
            </View>
        );
    }

    static propTypes = {
        style: PropTypes.object,
        onChoice: PropTypes.func.isRequired
    }
}

export default BooleanChoice;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { setAnswer } from '../actions/quizActions';
import BooleanChoice from './BooleanChoice';

class ChoiceComponent extends Component {

    _onChoice = (value) => {
        this.props.setAnswer(value);
        this.props.onPress();        
    }

    render() {
        let { question } = this.props;

        switch (question.type) {
        case 'boolean':
            return <BooleanChoice onChoice={this._onChoice} {...this.props}/>;
        default:
            return <BooleanChoice  onChoice={this._onChoice} {...this.props}/>;
        }
    }

    static propTypes = {
        question: PropTypes.object.isRequired,
        setAnswer: PropTypes.func.isRequired,
        onPress: PropTypes.func.isRequired
    }
}

const mapStateToProps = ({ quiz }) => ({
    quiz
});

const mapDispatchToProps = (dispatch) => ({
    setAnswer: (answer) => {
        dispatch(setAnswer(answer));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ChoiceComponent);

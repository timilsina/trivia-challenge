import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import HtmlEntity from '../components/HtmlEntity';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    scoreIcon: {
        fontSize: 24,
        marginRight: 8
    },
    questionText: {
        flex: 1,
        fontSize: 12,
        color: '#6B6666'
    },
    answerText: {
        borderRadius: 4,
        borderWidth: 1,
        paddingVertical: 2,
        paddingHorizontal: 6,
        marginTop: 8
    },
    correctAnswerText: {
        color: 'green',
        borderColor: 'green',
    },
    incorrectAnswerText: {
        color: 'red',
        borderColor: 'red',
    },
    answerSection: {
        marginLeft: 24,
        marginTop: 8
    },
    row: {
        flexDirection: 'row'
    }
});

class ResultRow extends Component {

    render() {
        let { style, question, active, onPress } = this.props;
        return (
            <TouchableOpacity onPress={onPress}>
                <View style={[styles.container, style]} >
                    <View style={styles.row}>
                        { question.score?
                            <Ionicons style={styles.scoreIcon} color='green' name='md-checkmark-circle' /> :
                            <Ionicons style={styles.scoreIcon} color='red' name='md-close-circle' />
                        }
                        <HtmlEntity style={styles.questionText} question={question.question} />
                    </View>
                    { active && 
                            <View style={styles.answerSection}>
                                <Text style={[styles.correctAnswerText, styles.answerText]}>Correct: {question.correct_answer}</Text>
                                { !question.score && <Text style={[styles.incorrectAnswerText, styles.answerText]}>Answered: {question.answer}</Text> }
                            </View>
                    }
                </View>
            </TouchableOpacity>
        );
    }

    static propTypes = {
        active: PropTypes.bool,
        style: PropTypes.number,
        onPress: PropTypes.func,
        question: PropTypes.object.isRequired,
    }

    static defaultProps = {
        active: false,
        onPress: () => {}
    }
}

export default ResultRow;

class Api {

    async getQuestions() {
        try {
            let response = await fetch('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean', 
                {
                    method: 'POST'
                });

            let responseJson = await response.json();
            return responseJson;
        } catch (error) {
            throw error;
        }
    }
}

export default new Api();

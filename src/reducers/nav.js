import AppNavigator from '../navigation/routes';
import { NavigationActions } from 'react-navigation';

const initialRouteName = 'Home';

const initialState = AppNavigator.router.getStateForAction(NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({
            routeName: initialRouteName,
        }),
    ],
}));

export default (state=initialState, action) => {
    return AppNavigator.router.getStateForAction(action, state);
};

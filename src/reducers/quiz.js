const initialState = {
    loading: true,
    error: null,
    questions: [],
    index: 0
};

import * as Actions from '../actions/quizActions';

export default (state=initialState, action) => {
    switch (action.type) {
    case Actions.SET_ANSWER:
    {
        let { index } = state;
        let question = state.questions[index];
        question.answer = action.payload;
        question.score = question.correct_answer.includes(action.payload);
        return state;
    }
    case Actions.SET_QUESTIONS:
        return { ...state, questions: action.payload, loading: false, index: 0 };
    case Actions.INCREASE_INDEX:
    {
        let index = state.index+1;
        return { ...state, index };
    }
    case Actions.SET_FETCHING:
        return initialState;
    default:
        return state;
    }
};

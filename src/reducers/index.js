import { combineReducers } from 'redux';
import nav from './nav';
import quiz from './quiz';

const appReducer = combineReducers({
    nav,
    quiz
});

// Setup root reducer
const rootReducer = (state, action) => {
    const newState = (action.type === 'RESET') ? undefined : state;
    return appReducer(newState, action);
};

export default rootReducer;

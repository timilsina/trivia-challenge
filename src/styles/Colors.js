export default {
    darkWhite: '#8f8f8f',
    lightWhite: '#4D4848',
    black: '#212121',
    lightBlack: '#424242',
    darkGrey: '#383838',
    darkerGrey: '#424242',
    white: '#FFFFFF',
};

import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

import AppNavigator from './navigation';
import { navMiddleware } from './utils/redux';

let middleware = [
    thunk,
    navMiddleware
];

if (__DEV__) {
    // Dev-only middleware
    middleware = [
        ...middleware,
        createLogger(), // Logs state changes to the dev console
    ];
}


export const store = compose(
    applyMiddleware(...middleware),
)(createStore)(rootReducer);

export default class AppContainer extends Component {

    render() {
        return (
            <Provider store={store}>
                <AppNavigator />
            </Provider>
        );
    }
}

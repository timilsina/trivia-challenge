import { StackNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import QuizScreen from '../screens/QuizScreen';
import ResultScreen from '../screens/ResultScreen';

export default StackNavigator({
    Home: { screen: HomeScreen },
    Quiz: { screen: QuizScreen },
    Result: { screen: ResultScreen }
}, {
    navigationOptions: {
        title: 'Trivia Challenge',
        headerTintColor: 'white',
        headerStyle: {
            elevation: 0,
            backgroundColor: '#03A9F4',
        },
    },
    headerMode: 'screen',
});

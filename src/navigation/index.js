import React, { Component } from 'react';
// eslint-disable-next-line react-native/split-platform-components
import { BackHandler, ToastAndroid } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';

import AppNavigator from './routes';
import { addListener } from '../utils/redux';
import EStyleSheet from 'react-native-extended-stylesheet';

EStyleSheet.build();

class AppWithNavigationState extends Component {

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

  onBackPress = () => {
      const { dispatch, nav } = this.props;

      if (nav.index === 0) {
          const currentTime = Date.now();
          if (currentTime - this.lastTime < 700) {
              return false;
          } else {
              this.lastTime = currentTime;
              ToastAndroid.show(
                  'Press again to exit',
                  ToastAndroid.SHORT
              );
              return true;
          }
      }
      dispatch(NavigationActions.back());
      return true;
  };

  render() {
      const { dispatch, nav } = this.props;

      return (
          <AppNavigator
              navigation={addNavigationHelpers({ 
                  dispatch,
                  state: nav,
                  addListener
              })} />
      );
  }
} 

AppWithNavigationState.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);

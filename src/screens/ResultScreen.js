import React, { Component } from 'react';
import { View, ScrollView, Image, StatusBar, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Card, Button, Text } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ResultRow from '../components/ResultRow';

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#28739e',
        justifyContent: 'center',
        alignItems: 'center'
    },
    scoreText: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        marginVertical: 3
    },
    scoreResultText: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        marginVertical: 3
    },
    closeIcon: {
        position: 'absolute',
        top: 24,
        left: 16,
        fontSize: 28,
        color: 'rgba(255, 255, 255, 0.6)',
    },
    cup: {
        height: 84,
        width: 84
    },
    separator: {
        height: 1,
        backgroundColor: 'rgba(40,115,14, 0.2)'
    },
    resultQuestions: {
        flex: 1,
        width: '100%-25',
    },
    resultCard: {
        padding: 8
    },
    playAgain: {
        marginVertical: 14,
        alignSelf: 'center'
    },
    playAgainText: {
        color: '#f8b64c',
        fontWeight: 'bold',
        fontSize: 16
    }
});

class ResultScreen extends Component {

    state = {
        activeSection: false,
        collapsed: true,
        score: 0
    }

    componentDidMount() {
        this._calculateScore(this.props.quiz.questions);
    }

    _calculateScore = (questions) => {
        let score = questions.filter(q => q.score).length;

        this.setState({ score });
    };

    _onPlayAgain = () => {
        this.props.navigation.navigate('Quiz');
    }

    _onItemPress = (index) => {
        if(this.state.activeSection === index) {
            index = -1;
        }
        this.setState({ activeSection: index });
    }

    _onClose = () => {
        this.props.navigation.navigate('Home');
    }

    render() {
        let { questions } = this.props.quiz;
        return (
            <View style={styles.container}>
                <StatusBar hidden={true} />
                <Ionicons  style={styles.closeIcon} name='md-close' onPress={this._onClose} />
                <View style={styles.score}>
                    <Image style={styles.cup} source={require('../assets/images/cup.png')} />
                    <Text style={styles.scoreText}>You Scored</Text>
                    <Text style={styles.scoreResultText}>{this.state.score}/10</Text>
                </View>
                <ScrollView style={styles.resultQuestions}
                    showsVerticalScrollIndicator={false} >
                    <Card style={styles.resultCard}>
                        <FlatList
                            extraData={this.state}
                            data={questions}
                            keyExtractor={(item, index) => index.toString()}
                            ItemSeparatorComponent={ () =>
                                <View style={styles.separator}/>
                            }
                            renderItem={({ item, index }) => (
                                <ResultRow question={item} 
                                    active={this.state.activeSection === index}
                                    onPress={() => this._onItemPress(index) } />
                            )} />
                    </Card>
                </ScrollView>
                <Button style={styles.playAgain}  light onPress={this._onPlayAgain} >
                    <Text style={styles.playAgainText}>PLAY AGAIN</Text>
                </Button>
            </View>
        );
    }

    static propTypes = {
        navigation: PropTypes.object.isRequired,
        quiz: PropTypes.object.isRequired
    }

    static navigationOptions = {
        header: null
    }
}

const mapStateToProps = ({ quiz }) => ({
    quiz
});

export default connect(mapStateToProps)(ResultScreen);

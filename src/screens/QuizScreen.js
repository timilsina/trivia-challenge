import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Alert } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import * as QuizActions from '../actions/quizActions';
import HtmlEntity from '../components/HtmlEntity'; import Choice from '../components/Choice';
import { Card, CardItem, Body, Icon } from 'native-base';

import Api from '../services/api';
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    top: {
        flex: 1,
        backgroundColor: '#28739e',
        width: '100%',
        flexBasis: '40%'
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        flexBasis: '60%'
    },
    closeIcon: {
        fontSize: 28,
        color: 'rgba(255, 255, 255, 0.6)',
        marginTop: 24,
        marginLeft: 16
    },
    paginationText: {
        color: '#817b7b',
        marginBottom: 16
    },
    categoryText: {
        alignSelf: 'center',
        color: '#ffffff',
        fontSize: 18,
        marginLeft: 15,
        marginTop: 10
    },
    question: {
        position: 'absolute',
        top: '20%',
        height: '60%',
        width: '80%',
    },
    questionBody: {
        alignItems: 'center',
    },
    questionText: {
        fontSize: 14,
        marginTop: 16,
        marginHorizontal: 6,
        height: 90
    }
});

class QuizScreen extends Component {

    _onNext = () => {
        let { index, questions } = this.props.quiz;

        if(questions.length > index+1) {
            this.props.increaseIndex();
        } else {
            const resetAction = NavigationActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home' }),
                    NavigationActions.navigate({ routeName: 'Result' })
                ]
            });
            this.props.navigation.dispatch(resetAction);
        }
    }

    async componentDidMount() {
        this._fetchQuestions();
    }

    _fetchQuestions = async () => {
        let { navigation } = this.props;
        this.props.setFetching();

        try {
            let response = await Api.getQuestions();
            let questions = response.results;
            this.props.setQuestions(questions);
        } catch(err) {

            Alert.alert(
                'Alert',
                'Error while fetching the questions.',
                [
                    { text: 'Back', onPress: () => navigation.goBack() },
                    { text: 'Try Again', onPress: () => this._fetchQuestions() }
                ]
            );
        }
    }

    _onClose = () => {
        let { navigation } = this.props;
        Alert.alert(
            'Confirm',
            'Do you want to exit from the quiz',
            [
                { text: 'Cancel', style: 'cancel' },
                { text: 'Ok', onPress: () => navigation.navigate('Home') }
            ]
        );
    }

    _renderQuestion = (question) => {
        return (
            <Card style={styles.question}>
                <CardItem>
                    {!this.props.quiz.loading?
                        <Body style={styles.questionBody}>
                            <HtmlEntity style={styles.questionText} question={question.question} />
                            <Choice style={styles.choic} onPress={this._onNext} question={question} />
                        </Body>
                        : <Text>loading</Text>}
                </CardItem>
            </Card>
        );
    }

    render() {
        let { questions, index, loading } = this.props.quiz;
        let question = questions[index];
        
        return (
            <View style={styles.container}>
                <StatusBar barStyle='light-content' hidden={false} />
                <View style={styles.top} >
                    <Icon  style={styles.closeIcon} name='md-close' onPress={this._onClose} />
                    {
                        question && <Text style={styles.categoryText}>{question.category}</Text>
                    }
                </View>
                { !loading &&
                        <View style={styles.bottom}>
                            <Text style={styles.paginationText}>{index+1} of {questions.length}</Text>
                        </View>
                }
                {this._renderQuestion(question)}
            </View>
        );
    }

    static propTypes = {
        navigation: PropTypes.object.isRequired,
        quiz: PropTypes.object.isRequired,
        increaseIndex: PropTypes.func.isRequired,
        setQuestions: PropTypes.func.isRequired,
        setFetching: PropTypes.func.isRequired
    }

    static navigationOptions = {
        header: null
    }
}

const mapStateToProps = ({ quiz }) => ({
    quiz
});

const mapDispatchToProps = (dispatch) => ({
    setQuestions: (questions) => {
        dispatch(QuizActions.setQuestions(questions));
    },
    increaseIndex: () => {
        dispatch(QuizActions.increaseIndex());
    },
    setFetching: () => {
        dispatch(QuizActions.setFetching());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizScreen);

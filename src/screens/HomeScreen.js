import React, { Component } from 'react';
import { View, StatusBar, Image } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Card, Text, Button } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#28739e',
        alignItems: 'center'
    },
    card: {
        width: '100%-80',
        marginTop: '5rem'
    },
    welcomeText: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 20
    },
    infoText: {
        textAlign: 'center',
        marginHorizontal: 12,
        fontSize: 16,
        color: '#817b7b',
        marginTop: '3rem'
    },
    beginButton: {
        alignSelf: 'center',
        marginVertical: 16
    },
    helpIcon: {
        fontSize: 48,
        color: '#4bb1ec'
    },
    challengeText: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 16,
        position: 'absolute',
        bottom: 24
    },
    topItem: {
        padding: 16,
        alignItems: 'center'
    },
    triviaImages: {
        position: 'absolute',
        alignItems: 'center',
        bottom: 4,
        left: 4,
        right: 4
    },
    image: {
        marginBottom: 4,
        resizeMode: 'stretch'
    }
});

class HomeScreen extends Component {

    _onBeginClick = () => {
        this.props.navigation.navigate('Quiz');
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle='light-content' hidden={false} />
                <Card style={styles.card}>
                    <View style={styles.topItem}>
                        <MaterialCommunityIcons style={styles.helpIcon} name='help-circle-outline' />
                        <Text style={styles.welcomeText}>WELCOME TO THE TRIVIA CHALLENGE</Text> 
                        <Text style={styles.infoText}>You will be presented with 10 True or False questions.</Text> 
                    </View>
                    <View style={styles.triviaImages}>
                        <Image style={styles.image} source={require('../assets/images/crown.png')} />
                        <Image style={styles.image} source={require('../assets/images/base.png')} />
                    </View>
                    <Text style={styles.challengeText}>Can you score 100%?</Text> 
                </Card>
                <Button small info style={styles.beginButton}  onPress={this._onBeginClick} > 
                    <Text>BEGIN</Text>
                </Button>
            </View>
        );
    }

  static navigationOptions = {
      header: null
  }

  static propTypes = {
      navigation: PropTypes.object.isRequired
  }
}

export default HomeScreen;

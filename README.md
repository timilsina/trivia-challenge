# Installation

Install node modules
`yarn`

## Running in android
`yarn android`

## Running in ios
`yarn ios`

## Generating icons
Change the logo file and run
`npm install -g yo generator-rn-toolbox`
`yo rn-toolbox:assets --icon logo.png`
